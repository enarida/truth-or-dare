//
//  ViewController.swift
//  Truth Or Dare
//
//  Created by Elisha Narida on 9/21/18.
//  Copyright © 2018 Elisha Narida. All rights reserved.
//

import UIKit
import FirebaseDatabase

class ViewController: UIViewController {

    @IBOutlet weak var btn_truth: UIButton!
    @IBOutlet weak var btn_dare: UIButton!
    @IBOutlet weak var btn_add: UIButton!
    @IBOutlet weak var txt_textView: UITextView!
    var tField: UITextField!
    var truths: [String] = []
    var dares: [String] = []
    
    var ref: DatabaseReference!
    var handler: DatabaseHandle!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = Database.database().reference()
        setupUI()
        
        handler = self.ref.child("Truth").observe(.childAdded, with: { (data) in
            if let item = data.value as? String { //converts the database value to string
                self.truths.append(item)
            }
        })
        
        handler = self.ref.child("Dare").observe(.childAdded, with: { (data) in
            if let item = data.value as? String { //converts the database value to string
                self.dares.append(item)
            }
        })
    }
    
    func setupUI () {
        btn_dare.layer.cornerRadius = 4
        btn_truth.layer.cornerRadius = 4
        btn_add.layer.cornerRadius = 4
        txt_textView.layer.cornerRadius = 10
        txt_textView.textContainerInset = UIEdgeInsetsMake(50, 20, 20, 50)
    }
    
    @IBAction func btn_truth_tpd(_ sender: Any) {
        print("truth tapped")
        txt_textView.backgroundColor = UIColor(hexString: "#56D6BA")
        txt_textView.text = self.truths.randomItem()
    }
    
    @IBAction func btn_dare_tpd(_ sender: Any) {
        print("dare tapped")
        txt_textView.backgroundColor = UIColor(hexString: "#F56C75")
        txt_textView.text = self.dares.randomItem()
    }
    
//    ADD
    
    @IBAction func btn_add_tpd(_ sender: Any) {
        print("add tapped")
        
        let add = UIAlertController(title: "Choose category", message: "Truth or Dare?", preferredStyle: .actionSheet)
        
        let truth = UIAlertAction(title: "Truth", style: .default) { (action:UIAlertAction) in
            print("truth tapped")
            self.addText(category: "Truth")
        }
        let dare = UIAlertAction(title: "Dare", style: .default) { (action:UIAlertAction) in
            print("dare tapped")
            self.addText(category: "Dare")
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .destructive) { (action:UIAlertAction) in
            print("cancel tapped")
        }
        
        add.addAction(truth)
        add.addAction(dare)
        add.addAction(cancel)
        self.present(add, animated: true, completion: nil)
    }
    
    func addText(category: String) {
        let alert = UIAlertController(title: "Add \(category)", message: "Isipin si kuya ver habang nag dadagdag ng \(category)", preferredStyle: .alert)
        
        alert.addTextField(configurationHandler: configurationTextField)
        
        let add = UIAlertAction(title: "Add", style: .default) { (result : UIAlertAction) -> Void in
            self.ref.child(category).childByAutoId().setValue(self.tField.text)
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        
        alert.addAction(cancel)
        alert.addAction(add)
        self.present(alert, animated: true, completion: nil)
    }
    
    func configurationTextField(textField: UITextField!){
        if (textField) != nil {
            tField = textField
            textField.placeholder = "add item"
        }else {
            print("no input added")
        }
    }

}

extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}

extension Array {
    func randomItem() -> Element? {
        if isEmpty { return nil }
        let index = Int(arc4random_uniform(UInt32(self.count)))
        return self[index]
    }
}

