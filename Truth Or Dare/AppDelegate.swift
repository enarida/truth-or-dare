//
//  AppDelegate.swift
//  Truth Or Dare
//
//  Created by Elisha Narida on 9/21/18.
//  Copyright © 2018 Elisha Narida. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        Auth.auth().signInAnonymously() { (authResult, error) in
            
        }
        return true
    }
}

